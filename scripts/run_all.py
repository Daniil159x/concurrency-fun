#!/usr/bin/env python3

import re
import sys
import subprocess

launchesNumber = 500

mods = (
    "Integer",
    "Float",
)

platforms = (
    "Seq",
    "SeqVector",
    "CPUThreads",
    "OpenCL",
)

methods = (
    "Addition",
    "DarkenOnly",
    "LightenOnly",
    "Multiply",
    "Subtract",
    "Screen",
    "Diff",
    "GrainExtract",
    "GrainMerge",
)


def run(pathToExec, maskFile, imageFile, targetDir):
    table = [("Method", "Mod", "Platform", "AverageTime", "TotalTime")]

    try:
        for method in methods:
            for mod in mods:
                for platform in platforms:
                    outputImage = f"{targetDir}/{method}_{mod}_{platform}.png"
                    args = [pathToExec, method, mod, platform, f"{launchesNumber}", maskFile, imageFile, outputImage]
                    res = subprocess.run(args, stderr=subprocess.STDOUT, stdout=subprocess.PIPE,
                                         universal_newlines=True)

                    if res.returncode != 0:
                        print(f"Method {method}, mod {mod}, platform {platform}:")
                        print(res.stdout)
                        raise Exception("Program returned a non-zero code")

                    totalTime = re.findall(r'([\d.]+)', res.stdout)[0]
                    totalTime = float(totalTime)
                    averageTime = totalTime / launchesNumber
                    table.append((method, mod, platform, averageTime, totalTime))
    except Exception as e:
        print(repr(e))
    finally:
        return table


def calcAlign(table):
    aligns = [0] * len(table[0])

    for i in range(len(aligns)):
        for j in range(len(table)):
            aligns[i] = max(aligns[i], len(str(table[j][i])))
        aligns[i] += 2
    return aligns


def printRow(align, row):
    s = '|' + '|'.join(map(lambda x: f"{{:{x}}}", align)) + '|'
    print(s.format(*row))


if __name__ == '__main__':
    pathToExec = sys.argv[1]
    maskFile = sys.argv[2]
    imageFile = sys.argv[3]
    targetDir = sys.argv[4]

    table = run(pathToExec, maskFile, imageFile, targetDir)
    align = calcAlign(table)

    printRow(align, table[0])
    printRow(align, list(map(lambda x: '-' * x, align)))
    for row in table[1:]:
        printRow(align, row)
