**concurrency-fun** - project for comparing parallelization methods using the example of overlapping two images.

[[_TOC_]]

## Details

Methods used from [gimp](https://docs.gimp.org/en/gimp-concepts-layer-modes.html) of overlapping images:

- Addition
- DarkenOnly
- LightenOnly
- Multiply
- Subtract
- Screen
- Diff
- GrainExtract
- GrainMerge

Methods implemented in two mods:

- Integer - 8-bit integer (range 0 - 255) is used for color
- Float - 32-bit floating-point number (range 0.0 - 1.0) is used for color

"Platform" means the possible implementation:

- Seq - sequence implementation (vectorisation is disabled, floats use standard 387)
- SeqVector - sequence implementation with AVX instructions
- CPUThreads - parallel implementation
- OpenCL - OpenCL implementation

## Result

### Sample

images: 2048 x 2048 24-bits

[Mask image](samples/big_mask1.png)

[Content image](samples/Lenna.png)

### Hardware and table

OS: Windows 10 64bit  
CPU: AMD Ryzen 5 3600  
OpenCL: AMD Radeon RX 5700 XT  
Compiler: MSVC 14.28

Launches number: 500. Time in milliseconds.

|Method        |Mod      |Platform    |AverageTime  |TotalTime  |
|--------------|---------|------------|-------------|-----------|
|Addition      |Integer  |Seq         |        8.016|     4008.0|
|Addition      |Integer  |SeqVector   |        1.006|      503.0|
|Addition      |Integer  |CPUThreads  |         1.21|      605.0|
|Addition      |Integer  |OpenCL      |         8.24|     4120.0|
|Addition      |Float    |Seq         |        6.002|     3001.0|
|Addition      |Float    |SeqVector   |         5.26|     2630.0|
|Addition      |Float    |CPUThreads  |          5.6|     2800.0|
|Addition      |Float    |OpenCL      |        27.96|    13980.0|
|DarkenOnly    |Integer  |Seq         |        6.026|     3013.0|
|DarkenOnly    |Integer  |SeqVector   |          1.0|      500.0|
|DarkenOnly    |Integer  |CPUThreads  |        1.038|      519.0|
|DarkenOnly    |Integer  |OpenCL      |        8.254|     4127.0|
|DarkenOnly    |Float    |Seq         |        8.012|     4006.0|
|DarkenOnly    |Float    |SeqVector   |        5.328|     2664.0|
|DarkenOnly    |Float    |CPUThreads  |        5.732|     2866.0|
|DarkenOnly    |Float    |OpenCL      |        27.85|    13925.0|
|LightenOnly   |Integer  |Seq         |          6.0|     3000.0|
|LightenOnly   |Integer  |SeqVector   |          1.0|      500.0|
|LightenOnly   |Integer  |CPUThreads  |        1.016|      508.0|
|LightenOnly   |Integer  |OpenCL      |        8.244|     4122.0|
|LightenOnly   |Float    |Seq         |        8.098|     4049.0|
|LightenOnly   |Float    |SeqVector   |        6.182|     3091.0|
|LightenOnly   |Float    |CPUThreads  |          6.0|     3000.0|
|LightenOnly   |Float    |OpenCL      |       27.958|    13979.0|
|Multiply      |Integer  |Seq         |          6.0|     3000.0|
|Multiply      |Integer  |SeqVector   |          1.0|      500.0|
|Multiply      |Integer  |CPUThreads  |        1.026|      513.0|
|Multiply      |Integer  |OpenCL      |        8.272|     4136.0|
|Multiply      |Float    |Seq         |        5.112|     2556.0|
|Multiply      |Float    |SeqVector   |        5.284|     2642.0|
|Multiply      |Float    |CPUThreads  |        5.508|     2754.0|
|Multiply      |Float    |OpenCL      |       27.868|    13934.0|
|Subtract      |Integer  |Seq         |        6.054|     3027.0|
|Subtract      |Integer  |SeqVector   |        1.006|      503.0|
|Subtract      |Integer  |CPUThreads  |         1.08|      540.0|
|Subtract      |Integer  |OpenCL      |         8.24|     4120.0|
|Subtract      |Float    |Seq         |        6.008|     3004.0|
|Subtract      |Float    |SeqVector   |         5.21|     2605.0|
|Subtract      |Float    |CPUThreads  |        5.662|     2831.0|
|Subtract      |Float    |OpenCL      |       27.936|    13968.0|
|Screen        |Integer  |Seq         |          7.0|     3500.0|
|Screen        |Integer  |SeqVector   |        1.008|      504.0|
|Screen        |Integer  |CPUThreads  |        1.084|      542.0|
|Screen        |Integer  |OpenCL      |         8.23|     4115.0|
|Screen        |Float    |Seq         |        5.172|     2586.0|
|Screen        |Float    |SeqVector   |        5.286|     2643.0|
|Screen        |Float    |CPUThreads  |        5.432|     2716.0|
|Screen        |Float    |OpenCL      |       28.036|    14018.0|
|Diff          |Integer  |Seq         |        2.026|     1013.0|
|Diff          |Integer  |SeqVector   |        1.004|      502.0|
|Diff          |Integer  |CPUThreads  |          1.0|      500.0|
|Diff          |Integer  |OpenCL      |        8.248|     4124.0|
|Diff          |Float    |Seq         |        5.098|     2549.0|
|Diff          |Float    |SeqVector   |        5.308|     2654.0|
|Diff          |Float    |CPUThreads  |        5.564|     2782.0|
|Diff          |Float    |OpenCL      |       27.942|    13971.0|
|GrainExtract  |Integer  |Seq         |         9.11|     4555.0|
|GrainExtract  |Integer  |SeqVector   |        1.012|      506.0|
|GrainExtract  |Integer  |CPUThreads  |        2.004|     1002.0|
|GrainExtract  |Integer  |OpenCL      |        8.244|     4122.0|
|GrainExtract  |Float    |Seq         |       13.012|     6506.0|
|GrainExtract  |Float    |SeqVector   |         5.47|     2735.0|
|GrainExtract  |Float    |CPUThreads  |        5.902|     2951.0|
|GrainExtract  |Float    |OpenCL      |        27.84|    13920.0|
|GrainMerge    |Integer  |Seq         |       10.212|     5106.0|
|GrainMerge    |Integer  |SeqVector   |        1.002|      501.0|
|GrainMerge    |Integer  |CPUThreads  |        2.016|     1008.0|
|GrainMerge    |Integer  |OpenCL      |         8.23|     4115.0|
|GrainMerge    |Float    |Seq         |       12.006|     6003.0|
|GrainMerge    |Float    |SeqVector   |        5.338|     2669.0|
|GrainMerge    |Float    |CPUThreads  |         5.87|     2935.0|
|GrainMerge    |Float    |OpenCL      |       27.916|    13958.0|


## Build

My conan profile `msvc-release`:

```yaml
[ settings ]
  os=Windows
  arch=x86_64
  compiler=Visual Studio
  compiler.version=16
  compiler.runtime=MD
  build_type=Release
```

Build commands:

```shell
mkdir .build
cd .build
conan install .. --install-folder=. -pr=msvc-release
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
```

If prebuild package `lodepng` doesn't exist, you can build locally. Just add the flag `--build=lodepng`
to `conan install`.

## Run

To run a single method, mod and platform:

```shell
.build/bin/main Addition Integer Seq 500 samples/mask2-dark.png samples/duck.png res.png
```

To run all methods, mods and platform:

```shell
python scripts/run_all.py .build/bin/main.exe samples/big_mask1.png samples/Lenna.png samples/res
```
