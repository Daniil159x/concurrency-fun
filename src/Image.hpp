#ifndef SRC_IMAGE_HPP
#define SRC_IMAGE_HPP

class Image
{
public:
    template<class T>
    struct c_unique_ptr
    {
        using deleter_t = void (*)(void*);

        c_unique_ptr(T *rowPtr, size_t size, deleter_t del)
            : data(rowPtr, del)
            , size(size)
        {
        }

        std::unique_ptr<T, deleter_t> data;
        size_t size;
    };

    explicit Image(std::string_view filepath);

    Image(Image && other) noexcept = default;
    Image(Image const & other);

    void SaveToFile(std::string_view filepath) const;

    [[nodiscard]] unsigned int Width( ) const;
    [[nodiscard]] unsigned int Height( ) const;

    [[nodiscard]] size_t DataSize() const;
    [[nodiscard]] unsigned char *Data() const;
    [[nodiscard]] c_unique_ptr<float> DataFloat() const;

    void SetDataFloat(c_unique_ptr<float> const & ptr);

private:
    c_unique_ptr<unsigned char> m_data;
    unsigned char * m_alignedData = nullptr;

    unsigned int m_width  = 0;
    unsigned int m_height = 0;
};

#endif //SRC_IMAGE_HPP
