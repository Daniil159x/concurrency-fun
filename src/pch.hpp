#ifndef SRC_PCH_HPP
#define SRC_PCH_HPP

// std includes
#include <filesystem>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <charconv>
#include <chrono>
#include <cstdlib>
#include <cassert>

// deps includes
#include <lib/Interface.hpp>
#include <lodepng.h>

// self includes
#include "cli.hpp"
#include "Image.hpp"
#include "Benchmark.hpp"

#endif //SRC_PCH_HPP
