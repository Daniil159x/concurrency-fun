#include "pch.hpp"
#include "cli.hpp"

enum CmdArgsIndexes
{
    Method = 1,
    Mod,
    Platform,
    LaunchesNumber,
    MaskImage,
    OriginImage,
    OutputImage,
    Number,
};

CmdArguments CmdArguments::Parse(const char * args[], size_t argc)
{
    if (argc != CmdArgsIndexes::Number)
    {
        throw std::invalid_argument("Incorrect argument number");
    }

    CmdArguments res;
    res.method   = Lib::MethodNameToId(args[Method]);
    res.mod      = Lib::ModNameToId(args[Mod]);
    res.platform = Lib::PlatformNameToId(args[Platform]);

    if (res.method == Lib::Methods::Unknown)
    {
        throw std::invalid_argument("Unknown method");
    }

    if (res.mod == Lib::Mods::Unknown)
    {
        throw std::invalid_argument("Unknown mod");
    }

    if (res.platform == Lib::Platforms::Unknown)
    {
        throw std::invalid_argument("Unknown platform");
    }

    auto launchesNumberStr = args[LaunchesNumber];
    auto [_, errc]         = std::from_chars(launchesNumberStr,
                                             launchesNumberStr + std::strlen(launchesNumberStr),
                                             res.launchesNumber);
    if (errc != std::errc())
    {
        auto err = std::make_error_condition(errc);
        throw std::invalid_argument(err.message());
    }

    res.maskImage   = args[MaskImage];
    res.originImage = args[OriginImage];
    res.outputImage = args[OutputImage];

    return res;
}

std::string Usage(const char *program)
{
    std::stringstream buf;

    buf << "Usage: " << std::filesystem::path(program).filename().string()
        << " method mod platform launchesNumber maskImage originImage outputImage\n";

    buf << "\nmethod:" << std::endl;
    for (auto &info : Lib::MethodsInfo)
    {
        buf << std::setw(15) << info.name << " : " << info.description << std::endl;
    }

    buf << "\nplatform:" << std::endl;
    for (auto &info : Lib::PlatformsInfo)
    {
        buf << std::setw(10) << info.name << " : " << info.description << std::endl;
    }

    buf << "\nmod:" << std::endl;
    for (auto &info : Lib::ModsInfo)
    {
        buf << std::setw(10) << info.name << " : " << info.description << std::endl;
    }

    buf << std::endl;
    buf << "launchesNumber - number of launches methods for benchmarks" << std::endl;
    buf << "maskImage - path to mask image " << std::endl;
    buf << "originImage - path to image file" << std::endl;
    buf << "outputImage - path to result (last) image file" << std::endl;
    buf << std::endl;

    return buf.str();
}
