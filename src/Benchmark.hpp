#ifndef SRC_BENCHMARK_HPP
#define SRC_BENCHMARK_HPP


class BenchmarkBase
{
public:
    explicit BenchmarkBase(CmdArguments const &args)
        : m_mask(args.maskImage)
        , m_image(args.originImage)
        , m_result(m_image)
        , m_launchCount(args.launchesNumber)
    {
        if (m_mask.Height() != m_image.Height() || m_mask.Width() != m_image.Width())
        {
            throw std::runtime_error("sizes of images are not equal");
        }
    }

    void SaveImageTo(std::string_view savePath) const { m_result.SaveToFile(savePath); }

protected:
    template<class Executor, class ImageDataPointer>
    size_t Run_(Executor &executor,
                ImageDataPointer down,
                ImageDataPointer up,
                ImageDataPointer result,
                size_t size)
    {
        std::chrono::high_resolution_clock::time_point t1, t2;
        size_t totalTime = 0;

        for (size_t i = 0; i < m_launchCount; ++i)
        {
            t1 = std::chrono::high_resolution_clock::now();
            executor.work(down, up, result, size);
            t2 = std::chrono::high_resolution_clock::now();

            auto time = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count();
            totalTime += static_cast<size_t>(time);
        }

        return totalTime;
    }

    Image m_mask;
    Image m_image;
    Image m_result;
    size_t m_launchCount;
};


template<Lib::Mods mod, Lib::Platforms platform, Lib::Methods method>
class Benchmark
{
public:
    static_assert(!(mod < Lib::Mods::Number), "Not implemented");
};

template<Lib::Platforms platform, Lib::Methods method>
class Benchmark<Lib::Mods::Integer, platform, method> : public BenchmarkBase
{
public:
    static constexpr const auto Mod = Lib::Mods::Integer;

    explicit Benchmark(CmdArguments const &args)
        : BenchmarkBase(args)
    {
    }

    size_t Run()
    {
        return Run_(m_executor, m_image.Data(), m_mask.Data(), m_result.Data(), m_result.DataSize());
    }

protected:
    Lib::Executor<method, Mod, platform> m_executor;
};

template<Lib::Platforms platform, Lib::Methods method>
class Benchmark<Lib::Mods::Float, platform, method> : public BenchmarkBase
{
public:
    static constexpr const auto Mod = Lib::Mods::Float;

    explicit Benchmark(CmdArguments const &args)
        : BenchmarkBase(args)
    {
    }

    size_t Run()
    {
        auto down = m_image.DataFloat();
        auto up   = m_mask.DataFloat();
        auto out  = m_result.DataFloat();

        auto time = Run_(m_executor, down.data.get(), up.data.get(), out.data.get(), out.size);

        m_result.SetDataFloat(out);

        return time;
    }

protected:
    Lib::Executor<method, Mod, platform> m_executor;
};

#endif //SRC_BENCHMARK_HPP
