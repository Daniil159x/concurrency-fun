#include "pch.hpp"
#include "Image.hpp"


// align to 64 byte cache line
constexpr static const std::size_t imageDataAlignment = 64;

inline bool isAligned(const void *ptr, std::uintptr_t alignment) noexcept
{
    auto iptr = reinterpret_cast<std::uintptr_t>(ptr);
    return !(iptr % alignment);
}

// std::aligned_malloc is missing in Microsoft STL
// but has _aligned_malloc and
#ifdef _MSC_VER
void *my_aligned_malloc(size_t alignment, size_t size)
{
    auto ptr = _aligned_malloc(size, alignment); // NOLINT(hicpp-no-malloc)
    if (ptr == nullptr)
    {
        throw std::bad_alloc();
    }
    return ptr;
}
void my_aligned_free(void *memblock)
{
    _aligned_free(memblock); // NOLINT(hicpp-no-malloc)
}
#else
void *my_aligned_malloc(size_t alignment, size_t size)
{
    auto ptr = std::aligned_alloc(alignment, size); // NOLINT(hicpp-no-malloc)
    if (ptr == nullptr)
    {
        throw std::bad_alloc();
    }
    return ptr;
}
void my_aligned_free(void *memblock)
{
    std::free(memblock); // NOLINT(hicpp-no-malloc)
}
#endif

template<class T>
Image::c_unique_ptr<T> make_c_unique_ptr(size_t size)
{
    auto rowPtr  = my_aligned_malloc(imageDataAlignment, sizeof(T) * size);
    auto rowPtrT = static_cast<T *>(rowPtr);
    return { rowPtrT, size, &my_aligned_free };
}

Image::Image(std::string_view filepath)
    : m_data{ nullptr, 0, &std::free }
{
    unsigned char *out = nullptr;

    auto err = lodepng_decode24_file(&out, &m_width, &m_height, filepath.data());
    if (err)
    {
        throw std::runtime_error("opening image from '" + std::string(filepath)
                                 + "': " + lodepng_error_text(err));
    }

    m_data.data.reset(out);
    m_data.size = m_width * m_height * 3;

    if (m_width != m_height)
    {
        throw std::runtime_error("incorrect size of image '" + std::string(filepath)
                                 + "', need square");
    }

    if (isAligned(m_data.data.get(), imageDataAlignment))
    {
        m_alignedData = m_data.data.get();
        return;
    }


    auto newData = make_c_unique_ptr<unsigned char>(m_data.size);
    m_alignedData = newData.data.get();

    std::memcpy(m_alignedData, m_data.data.get(), m_data.size);

    m_data = std::move(newData);
}

Image::Image(Image const &other)
    : m_data{ nullptr, other.m_data.size, &my_aligned_free }
    , m_width(other.m_width)
    , m_height(other.m_height)
{
    m_data = make_c_unique_ptr<unsigned char>(other.m_data.size);
    m_alignedData = m_data.data.get();

    std::memcpy(m_alignedData, other.m_alignedData, m_data.size);
}

void Image::SaveToFile(std::string_view filepath) const
{
    auto err = lodepng_encode24_file(filepath.data(), m_alignedData, m_width, m_height);
    if (err)
    {
        throw std::runtime_error("saving image to '" + std::string(filepath)
                                 + "': " + lodepng_error_text(err));
    }
}

unsigned int Image::Width() const
{
    return m_width;
}

unsigned int Image::Height() const
{
    return m_height;
}

size_t Image::DataSize() const
{
    return m_data.size;
}

unsigned char *Image::Data() const
{
    return m_alignedData;
}

Image::c_unique_ptr<float> Image::DataFloat() const
{
    auto floatData = make_c_unique_ptr<float>(m_data.size);

    for (size_t i = 0; i < m_data.size; ++i)
    {
        floatData.data.get()[i] = static_cast<float>(m_alignedData[i]) / 255.0F;
    }

    return floatData;
}

void Image::SetDataFloat(c_unique_ptr<float> const & floatData)
{
    if( floatData.size != m_data.size )
    {
        m_data = make_c_unique_ptr<unsigned char>(floatData.size);
    }

    for (size_t i = 0; i < floatData.size; ++i)
    {
        m_data.data.get()[i] = static_cast<unsigned char>(floatData.data.get()[i] * 255.0F);
    }
}
