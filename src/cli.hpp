#ifndef SRC_CLI_HPP
#define SRC_CLI_HPP

struct CmdArguments
{
    Lib::Methods method          = {};
    Lib::Platforms platform      = {};
    Lib::Mods mod                = {};
    size_t launchesNumber        = {};
    std::string_view maskImage   = {};
    std::string_view originImage = {};
    std::string_view outputImage = {};

    static CmdArguments Parse(const char * args[], size_t argc);
};

std::string Usage(const char *program);

#endif //SRC_CLI_HPP
