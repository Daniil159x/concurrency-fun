cmake_minimum_required(VERSION 3.17)
project(main)

add_executable(${PROJECT_NAME} main.cpp cli.hpp cli.cpp pch.hpp Image.cpp Image.hpp Benchmark.hpp)

target_link_libraries(${PROJECT_NAME} CONAN_PKG::lodepng lib)
