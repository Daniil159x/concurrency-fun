#include "pch.hpp"

template<Lib::Mods mod, Lib::Platforms platform, Lib::Methods method>
void benchmark(CmdArguments const &args)
{
    Benchmark<mod, platform, method> bench(args);

    auto totalTime = bench.Run();

    std::cout << "totalTime: " << totalTime << std::endl;

    bench.SaveImageTo(args.outputImage);
}

template<Lib::Mods mod, Lib::Platforms platform>
void routeMethod(CmdArguments const &args)
{
    // clang-format off
    switch (args.method)
    {
    case Lib::Methods::Addition:     return benchmark<mod, platform, Lib::Methods::Addition>(args);
    case Lib::Methods::DarkenOnly:   return benchmark<mod, platform, Lib::Methods::DarkenOnly>(args);
    case Lib::Methods::LightenOnly:  return benchmark<mod, platform, Lib::Methods::LightenOnly>(args);
    case Lib::Methods::Multiply:     return benchmark<mod, platform, Lib::Methods::Multiply>(args);
    case Lib::Methods::Subtract:     return benchmark<mod, platform, Lib::Methods::Subtract>(args);
    case Lib::Methods::Screen:       return benchmark<mod, platform, Lib::Methods::Screen>(args);
    case Lib::Methods::Diff:         return benchmark<mod, platform, Lib::Methods::Diff>(args);
    case Lib::Methods::GrainExtract: return benchmark<mod, platform, Lib::Methods::GrainExtract>(args);
    case Lib::Methods::GrainMerge:   return benchmark<mod, platform, Lib::Methods::GrainMerge>(args);
    default:
        throw std::invalid_argument("Unknown method");
    }
    // clang-format on
}

template<Lib::Mods mod>
void routePlatform(CmdArguments const &args)
{
    // clang-format off
    switch (args.platform)
    {
    case Lib::Platforms::Seq:        return routeMethod<mod, Lib::Platforms::Seq>(args);
    case Lib::Platforms::SeqVector:  return routeMethod<mod, Lib::Platforms::SeqVector>(args);
    case Lib::Platforms::CPUThreads: return routeMethod<mod, Lib::Platforms::CPUThreads>(args);
    case Lib::Platforms::OpenCL:     return routeMethod<mod, Lib::Platforms::OpenCL>(args);
    default:
        throw std::invalid_argument("Unknown platform");
    }
    // clang-format on
}

void run(CmdArguments const &args)
{
    // clang-format off
    switch (args.mod)
    {
    case Lib::Mods::Integer: return routePlatform<Lib::Mods::Integer>(args);
    case Lib::Mods::Float:   return routePlatform<Lib::Mods::Float>(args);
    default:
        throw std::invalid_argument("Unknown mod");
    }
    // clang-format on
}

int main(int argc, const char *argv[])
{
    try
    {
        auto args = CmdArguments::Parse(argv, static_cast<size_t>(argc));
        run(args);
    }
    catch (cl::BuildError const &e)
    {
        std::cerr << "Build OpenCl error: " << e.what() << std::endl;
        for(auto &[dev, log] : e.getBuildLog())
        {
            std::cerr << dev.getInfo<CL_DEVICE_NAME>() << std::endl;
            std::cerr << log << std::endl;
        }
        return -1;
    }
    catch (cl::Error const &e)
    {
        std::cerr << "OpenCl exception with code = " << e.err() << " :" << e.what() << std::endl;
        return -1;
    }
    catch (std::invalid_argument const &e)
    {
        std::cerr << "Error occurred: " << e.what() << std::endl;
        std::cerr << Usage(argv[0]) << std::endl;
        return -1;
    }
    catch (std::exception const &e)
    {
        std::cerr << "Error occurred: " << e.what() << std::endl;
        return -1;
    }
    catch (...)
    {
        std::cerr << "Unexpected error occurred" << std::endl;
        return -1;
    }

    return 0;
}
