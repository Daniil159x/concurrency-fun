#ifndef LIB_MODS_HPP
#define LIB_MODS_HPP

namespace Lib
{
    enum class Mods : size_t
    {
        Integer,
        Float,
        Number,
        Unknown = Number,
    };

    struct ModInfo
    {
        std::string_view name;
        std::string_view description;
    };

    constexpr auto MakeModsInfo()
    {
        std::array<ModInfo, static_cast<size_t>(Mods::Number)> res = {};

        res[static_cast<size_t>(Mods::Integer)] = { "Integer", "TODO" };
        res[static_cast<size_t>(Mods::Float)]   = { "Float", "TODO" };

        return res;
    }

    static constexpr const auto ModsInfo = MakeModsInfo();

    inline Mods ModNameToId(std::string_view name)
    {
        auto platformIt = std::find_if(ModsInfo.begin(), ModsInfo.end(), [&](ModInfo const &info) {
            return info.name == name;
        });
        return static_cast<Mods>(std::distance(ModsInfo.begin(), platformIt));
    }

    template<Mods mod>
    struct ImageTraits
    {
        using ColorType = unsigned char;
    };

    template<>
    struct ImageTraits<Mods::Float>
    {
        using ColorType = float;
    };

}

#endif //LIB_MODS_HPP
