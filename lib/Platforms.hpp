#ifndef LIB_PLATFORM_HPP
#define LIB_PLATFORM_HPP

namespace Lib
{
    enum class Platforms : size_t
    {
        Seq,
        SeqVector,
        CPUThreads,
        OpenCL,
        Number,
        Unknown = Number,
    };

    struct PlatformInfo
    {
        std::string_view name;
        std::string_view description;
    };

    constexpr auto MakePlatformsInfo()
    {
        std::array<PlatformInfo, static_cast<size_t>(Platforms::Number)> res = {};

        res[static_cast<size_t>(Platforms::Seq)]        = { "Seq", "TODO" };
        res[static_cast<size_t>(Platforms::SeqVector)]  = { "SeqVector", "TODO" };
        res[static_cast<size_t>(Platforms::CPUThreads)] = { "CPUThreads", "TODO" };
        res[static_cast<size_t>(Platforms::OpenCL)]     = { "OpenCL", "TODO" };

        return res;
    }

    static constexpr const auto PlatformsInfo = MakePlatformsInfo();

    inline Platforms PlatformNameToId(std::string_view name)
    {
        auto platformIt = std::find_if(PlatformsInfo.begin(),
                                       PlatformsInfo.end(),
                                       [&](PlatformInfo const &info) { return info.name == name; });
        return static_cast<Platforms>(std::distance(PlatformsInfo.begin(), platformIt));
    }
}

#endif //LIB_PLATFORM_HPP
