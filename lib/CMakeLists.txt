cmake_minimum_required(VERSION 3.17)
project(lib)

add_library(${PROJECT_NAME}
        Interface.hpp
        Methods.hpp
        Mods.hpp
        Platforms.hpp
        Executor.hpp
        Executors/SeqExecutor.cpp
        Executors/SeqExecutor.hpp
        Executors/SeqVecExecutor.cpp
        Executors/SeqVecExecutor.hpp
        Executors/CPUThreadsExecutor.hpp
        Executors/OpenCLExecutor.hpp
        Executors/OpenCLExecutor.cpp
        )

target_compile_definitions(${PROJECT_NAME}
        PUBLIC
        CL_HPP_ENABLE_EXCEPTIONS
        CL_HPP_TARGET_OPENCL_VERSION=200
        )

target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/..)
target_link_libraries(${PROJECT_NAME} PUBLIC CONAN_PKG::khronos-opencl-clhpp)

if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND CMAKE_CXX_COMPILER_FRONTEND_VARIANT STREQUAL "GNU" OR CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    set(DISABLE_VECTORIZATION
            -mfpmath=387
            -mno-avx
            -mno-avx2
            )

    if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
        list(APPEND DISABLE_VECTORIZATION
                -fno-slp-vectorize
                -fno-vectorize
                -mno-sse
                )
    else ()
        list(APPEND DISABLE_VECTORIZATION -fno-tree-vectorize)
    endif ()

    set(ENABLE_AVX2
            -mavx2
            -mavx
            )

    set_source_files_properties(Executors/SeqVecExecutor.cpp PROPERTIES COMPILE_OPTIONS "${ENABLE_AVX2}")
    set_source_files_properties(Executors/SeqExecutor.cpp PROPERTIES COMPILE_OPTIONS "${DISABLE_VECTORIZATION}")

elseif (CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
    # for MSVC 'pragma loop(no_vector)' is used
    set_source_files_properties(Executors/SeqVecExecutor.cpp PROPERTIES COMPILE_OPTIONS "/arch:AVX2")

else ()
    message(WARNING "Vectorization isn't disable for compiler ${CMAKE_CXX_COMPILER_ID}")
endif ()
