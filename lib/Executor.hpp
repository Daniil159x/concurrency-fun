#ifndef LIB_EXECUTOR_HPP
#define LIB_EXECUTOR_HPP

namespace Lib
{
    template<Methods method, Mods mod, Platforms platform>
    class Executor
    {
    public:
        using Pointer      = typename ImageTraits<mod>::ColorType *;
        using ConstPointer = const typename ImageTraits<mod>::ColorType *;

        void work(ConstPointer /*image*/,
                  ConstPointer /*mask*/,
                  Pointer /*outImage*/,
                  std::size_t /*size*/)
        {
            static_assert(!(method < Methods::Number || mod < Mods::Number
                            || platform < Platforms::Number),
                          "Not implemented");
        }
    };
}

#endif //LIB_EXECUTOR_HPP
