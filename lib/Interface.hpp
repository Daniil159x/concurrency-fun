#ifndef LIB_INTERFACE_HPP
#define LIB_INTERFACE_HPP

// std headers
#include <algorithm>
#include <array>
#include <string_view>
#include <vector>
#include <thread>
#include <stdexcept>

// opencl
#include <CL/cl2.hpp>

// lib headers
#include "Methods.hpp"
#include "Platforms.hpp"
#include "Mods.hpp"

#include "Executor.hpp"
#include "Executors/SeqExecutor.hpp"
#include "Executors/SeqVecExecutor.hpp"
#include "Executors/CPUThreadsExecutor.hpp"
#include "Executors/OpenCLExecutor.hpp"

#endif //LIB_INTERFACE_HPP
