#ifndef LIB_METHODS_HPP
#define LIB_METHODS_HPP

namespace Lib
{
    enum class Methods : size_t
    {
        Addition,
        DarkenOnly,
        LightenOnly,
        Multiply,
        Subtract,
        Screen,
        Diff,
        GrainExtract,
        GrainMerge,
        Number,
        Unknown = Number,
    };

    struct MethodInfo
    {
        std::string_view name;
        std::string_view description;
    };

    constexpr auto MakeMethodsInfo()
    {
        std::array<MethodInfo, static_cast<size_t>(Methods::Number)> res = {};

        res[static_cast<size_t>(Methods::Addition)]     = { "Addition", "TODO" };
        res[static_cast<size_t>(Methods::DarkenOnly)]   = { "DarkenOnly", "TODO" };
        res[static_cast<size_t>(Methods::LightenOnly)]  = { "LightenOnly", "TODO" };
        res[static_cast<size_t>(Methods::Multiply)]     = { "Multiply", "TODO" };
        res[static_cast<size_t>(Methods::Subtract)]     = { "Subtract", "TODO" };
        res[static_cast<size_t>(Methods::Screen)]       = { "Screen", "TODO" };
        res[static_cast<size_t>(Methods::Diff)]         = { "Diff", "TODO" };
        res[static_cast<size_t>(Methods::GrainExtract)] = { "GrainExtract", "TODO" };
        res[static_cast<size_t>(Methods::GrainMerge)]   = { "GrainMerge", "TODO" };

        return res;
    }

    static constexpr const auto MethodsInfo = MakeMethodsInfo();

    inline Methods MethodNameToId(std::string_view name)
    {
        auto methodIt = std::find_if(MethodsInfo.begin(),
                                     MethodsInfo.end(),
                                     [&](MethodInfo const &info) { return info.name == name; });
        return static_cast<Methods>(std::distance(MethodsInfo.begin(), methodIt));
    }
}

#endif //LIB_METHODS_HPP
