#ifndef LIB_EXECUTORS_SEQEXECUTOR_HPP
#define LIB_EXECUTORS_SEQEXECUTOR_HPP

#define DefineSeqExecutor(Mod, Method)                                                             \
    template<>                                                                                     \
    class Executor<Method, Mod, Platforms::Seq>                                                    \
    {                                                                                              \
    public:                                                                                        \
        using Pointer      = ImageTraits<Mod>::ColorType *;                                        \
        using ConstPointer = const ImageTraits<Mod>::ColorType *;                                  \
        void work(ConstPointer image, ConstPointer mask, Pointer outImage, std::size_t size);      \
    }


namespace Lib
{
    DefineSeqExecutor(Mods::Integer, Methods::Addition);
    DefineSeqExecutor(Mods::Integer, Methods::DarkenOnly);
    DefineSeqExecutor(Mods::Integer, Methods::LightenOnly);
    DefineSeqExecutor(Mods::Integer, Methods::Multiply);
    DefineSeqExecutor(Mods::Integer, Methods::Subtract);
    DefineSeqExecutor(Mods::Integer, Methods::Screen);
    DefineSeqExecutor(Mods::Integer, Methods::Diff);
    DefineSeqExecutor(Mods::Integer, Methods::GrainExtract);
    DefineSeqExecutor(Mods::Integer, Methods::GrainMerge);

    DefineSeqExecutor(Mods::Float, Methods::Addition);
    DefineSeqExecutor(Mods::Float, Methods::DarkenOnly);
    DefineSeqExecutor(Mods::Float, Methods::LightenOnly);
    DefineSeqExecutor(Mods::Float, Methods::Multiply);
    DefineSeqExecutor(Mods::Float, Methods::Subtract);
    DefineSeqExecutor(Mods::Float, Methods::Screen);
    DefineSeqExecutor(Mods::Float, Methods::Diff);
    DefineSeqExecutor(Mods::Float, Methods::GrainExtract);
    DefineSeqExecutor(Mods::Float, Methods::GrainMerge);
}

#undef DefineExecutor

#endif //LIB_EXECUTORS_SEQEXECUTOR_HPP
