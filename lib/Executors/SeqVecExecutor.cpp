#include "../Interface.hpp"
#include <immintrin.h>

using namespace Lib;

#define BeginSeqVectorIntegerExecutorWork(Method)                                                  \
    void Executor<Method, Mods::Integer, Platforms::SeqVector>::work(Executor::ConstPointer image, \
                                                                     Executor::ConstPointer mask,  \
                                                                     Executor::Pointer out,        \
                                                                     std::size_t size)             \
    { /* it save use in range (0, AVX2_Size) because data is aligned to 64 byte */                 \
        auto AVX2_Mask  = reinterpret_cast<const __m256i *>(mask);                                 \
        auto AVX2_Image = reinterpret_cast<const __m256i *>(image);                                \
        auto AVX2_Out   = reinterpret_cast<__m256i *>(out);                                        \
                                                                                                   \
        constexpr const size_t ratio = 32;                                                         \
        auto AVX2_Size               = size / ratio;


#define EndSeqVectorIntegerExecutorWork                                                            \
    auto offset = AVX2_Size * ratio;                                                               \
    BaseExecutor::work(image + offset, mask + offset, out + offset, size - offset);                \
    }

#define BeginSeqVectorFloatExecutorWork(Method)                                                    \
    void Executor<Method, Mods::Float, Platforms::SeqVector>::work(Executor::ConstPointer image, \
                                                                     Executor::ConstPointer mask,  \
                                                                     Executor::Pointer out,        \
                                                                     std::size_t size)             \
    { /* it save use in range (0, AVX_Size) because data is aligned to 64 byte */                  \
        auto AVX_Mask  = reinterpret_cast<const __m256 *>(mask);                                   \
        auto AVX_Image = reinterpret_cast<const __m256 *>(image);                                  \
        auto AVX_Out   = reinterpret_cast<__m256 *>(out);                                          \
                                                                                                   \
        constexpr const size_t ratio = 8;                                                          \
        auto AVX_Size                = size / ratio;

#define EndSeqVectorFloatExecutorWork                                                              \
    auto offset = AVX_Size * ratio;                                                                \
    BaseExecutor::work(image + offset, mask + offset, out + offset, size - offset);                \
    }


enum class Part : int
{
    Low  = 0,
    High = 1,
};

template<Part m>
inline __m256i extend_u8_to_i16(__m256i a)
{
    return _mm256_cvtepu8_epi16(_mm256_extracti128_si256(a, static_cast<int>(m)));
}

inline __m256i shorten_i16_to_u8(__m256i low, __m256i high)
{
    __m256i shorten = _mm256_packus_epi16(low, high);
    return _mm256_permute4x64_epi64(shorten, 0b11'01'10'00);
}

inline __m256 clamp_ps(__m256 a, __m256 min, __m256 max)
{
    return _mm256_max_ps(_mm256_min_ps(max, a), min);
}

// ---------------------   Mods::Integer ----------------------------

BeginSeqVectorIntegerExecutorWork(Methods::Addition)
{
    for (size_t i = 0; i < AVX2_Size; ++i)
    {
        AVX2_Out[i] = _mm256_adds_epu8(AVX2_Mask[i], AVX2_Image[i]);
    }
}
EndSeqVectorIntegerExecutorWork

BeginSeqVectorIntegerExecutorWork(Methods::DarkenOnly)
{
    for (size_t i = 0; i < AVX2_Size; ++i)
    {
        AVX2_Out[i] = _mm256_min_epu8(AVX2_Mask[i], AVX2_Image[i]);
    }
}
EndSeqVectorIntegerExecutorWork

BeginSeqVectorIntegerExecutorWork(Methods::LightenOnly)
{
    for (size_t i = 0; i < AVX2_Size; ++i)
    {
        AVX2_Out[i] = _mm256_max_epu8(AVX2_Mask[i], AVX2_Image[i]);
    }
}
EndSeqVectorIntegerExecutorWork

BeginSeqVectorIntegerExecutorWork(Methods::Multiply)
{
    __m256i zero = _mm256_set1_epi64x(0);
    __m256i ff = _mm256_set1_epi64x(static_cast<long long int>(0xFFFFFFFFFFFFFFFF));
    __m256i mul_mask = _mm256_unpacklo_epi8(ff, zero);

    __m256i denominator = _mm256_set1_epi16(32768 / 255);

    for (size_t i = 0; i < AVX2_Size; ++i)
    {
        // zero extend 8 bit to 16 bit
        __m256i first_16_byte_mask  = extend_u8_to_i16<Part::Low>(AVX2_Mask[i]);
        __m256i first_16_byte_image = extend_u8_to_i16<Part::Low>(AVX2_Image[i]);

        // I * M
        __m256i multi_first_16_byte = _mm256_mullo_epi16(first_16_byte_mask, first_16_byte_image);

        // I * M / 255
        __m256i result_first_16_byte = _mm256_mulhrs_epi16(multi_first_16_byte, denominator);
        result_first_16_byte = _mm256_and_si256(result_first_16_byte, mul_mask);

        // zero extend 8 bit to 16 bit
        __m256i second_16_byte_mask  = extend_u8_to_i16<Part::High>(AVX2_Mask[i]);
        __m256i second_16_byte_image = extend_u8_to_i16<Part::High>(AVX2_Image[i]);

        // I * M
        __m256i multi_second_16_byte = _mm256_mullo_epi16(second_16_byte_mask, second_16_byte_image);

        // I * M / 255
        __m256i result_second_16_byte = _mm256_mulhrs_epi16(multi_second_16_byte, denominator);
        result_second_16_byte = _mm256_and_si256(result_second_16_byte, mul_mask);

        // convert 16 bit to 8 bit
        AVX2_Out[i] = shorten_i16_to_u8(result_first_16_byte, result_second_16_byte);
    }
}
EndSeqVectorIntegerExecutorWork

BeginSeqVectorIntegerExecutorWork(Methods::Subtract)
{
    for (size_t i = 0; i < AVX2_Size; ++i)
    {
        AVX2_Out[i] = _mm256_subs_epu8(AVX2_Image[i], AVX2_Mask[i]);
    }
}
EndSeqVectorIntegerExecutorWork

BeginSeqVectorIntegerExecutorWork(Methods::Screen)
{
    __m256i zero = _mm256_set1_epi64x(0);
    __m256i ff = _mm256_set1_epi64x(static_cast<long long int>(0xFFFFFFFFFFFFFFFF));
    __m256i mul_mask = _mm256_unpacklo_epi8(ff, zero);

    __m256i denominator = _mm256_set1_epi16(32768 / 255);

    for (size_t i = 0; i < AVX2_Size; ++i)
    {
        // 255 - M
        __m256i MM = _mm256_subs_epu8(ff, AVX2_Mask[i]);

        // 255 - I
        __m256i II = _mm256_subs_epu8(ff, AVX2_Image[i]);

        // ---------- low ---------
        __m256i first_16_byte_MM = extend_u8_to_i16<Part::Low>(MM);
        __m256i first_16_byte_II = extend_u8_to_i16<Part::Low>(II);

        // (255 - M) * (255 - I)
        __m256i multi_first_16_byte = _mm256_mullo_epi16(first_16_byte_MM, first_16_byte_II);

        // (255 - M) * (255 - I) / 255
        __m256i fraction_first_16_byte = _mm256_mulhrs_epi16(multi_first_16_byte, denominator);
        fraction_first_16_byte = _mm256_and_si256(fraction_first_16_byte, mul_mask);

        // ---------- high ---------
        __m256i second_16_byte_MM = extend_u8_to_i16<Part::High>(MM);
        __m256i second_16_byte_II = extend_u8_to_i16<Part::High>(II);

        // (255 - M) * (255 - I)
        __m256i multi_second_16_byte = _mm256_mullo_epi16(second_16_byte_MM, second_16_byte_II);

        // (255 - M) * (255 - I) / 255
        __m256i fraction_second_16_byte = _mm256_mulhrs_epi16(multi_second_16_byte, denominator);
        fraction_second_16_byte = _mm256_and_si256(fraction_second_16_byte, mul_mask);

        // ---------- all ---------
        __m256i fraction = shorten_i16_to_u8(fraction_first_16_byte, fraction_second_16_byte);

        // 255 - (255 - M) * (255 - I) / 255
        AVX2_Out[i] = _mm256_subs_epu8(ff, fraction);
    }
}
EndSeqVectorIntegerExecutorWork

BeginSeqVectorIntegerExecutorWork(Methods::Diff)
{
    for (size_t i = 0; i < AVX2_Size; ++i)
    {
        // ---------- low ---------
        __m256i M_low = extend_u8_to_i16<Part::Low>(AVX2_Mask[i]);
        __m256i I_low = extend_u8_to_i16<Part::Low>(AVX2_Image[i]);

        __m256i diff_low = _mm256_subs_epi16(I_low, M_low);
        __m256i abs_low = _mm256_abs_epi16(diff_low);

        // ---------- high ---------
        __m256i M_high = extend_u8_to_i16<Part::High>(AVX2_Mask[i]);
        __m256i I_high = extend_u8_to_i16<Part::High>(AVX2_Image[i]);

        __m256i diff_high = _mm256_subs_epi16(I_high, M_high);
        __m256i abs_high = _mm256_abs_epi16(diff_high);

        AVX2_Out[i] = shorten_i16_to_u8(abs_low, abs_high);
    }
}
EndSeqVectorIntegerExecutorWork

BeginSeqVectorIntegerExecutorWork(Methods::GrainExtract)
{
    __m256i add_128 = _mm256_set1_epi16 (128);

    for (size_t i = 0; i < AVX2_Size; ++i)
    {
        // ---------- low ---------
        __m256i M_low = extend_u8_to_i16<Part::Low>(AVX2_Mask[i]);
        __m256i I_low = extend_u8_to_i16<Part::Low>(AVX2_Image[i]);

        // I - M + 128
        __m256i result_low = _mm256_add_epi16(_mm256_sub_epi16(I_low, M_low), add_128);

        // ---------- high ---------
        __m256i M_high = extend_u8_to_i16<Part::High>(AVX2_Mask[i]);
        __m256i I_high = extend_u8_to_i16<Part::High>(AVX2_Image[i]);

        // I - M + 128
        __m256i result_high = _mm256_add_epi16(_mm256_sub_epi16(I_high, M_high), add_128);

        AVX2_Out[i] = shorten_i16_to_u8(result_low, result_high);
    }
}
EndSeqVectorIntegerExecutorWork

BeginSeqVectorIntegerExecutorWork(Methods::GrainMerge)
{
    __m256i add_128 = _mm256_set1_epi16 (128);

    for (size_t i = 0; i < AVX2_Size; ++i)
    {
        // ---------- low ---------
        __m256i M_low = extend_u8_to_i16<Part::Low>(AVX2_Mask[i]);
        __m256i I_low = extend_u8_to_i16<Part::Low>(AVX2_Image[i]);

        // I + M - 128
        __m256i result_low = _mm256_sub_epi16(_mm256_add_epi16(I_low, M_low), add_128);

        // ---------- high ---------
        __m256i M_high = extend_u8_to_i16<Part::High>(AVX2_Mask[i]);
        __m256i I_high = extend_u8_to_i16<Part::High>(AVX2_Image[i]);

        // I + M - 128
        __m256i result_high = _mm256_sub_epi16(_mm256_add_epi16(I_high, M_high), add_128);

        AVX2_Out[i] = shorten_i16_to_u8(result_low, result_high);
    }
}
EndSeqVectorIntegerExecutorWork

// ---------------------   Mods::Float ----------------------------

BeginSeqVectorFloatExecutorWork(Methods::Addition)
{
    __m256 one = _mm256_set1_ps(1.0F);

    for (size_t i = 0; i < AVX_Size; ++i)
    {
        __m256 sum = _mm256_add_ps(AVX_Mask[i], AVX_Image[i]);
        AVX_Out[i] = _mm256_min_ps(sum, one);
    }
}
EndSeqVectorFloatExecutorWork

BeginSeqVectorFloatExecutorWork(Methods::DarkenOnly)
{
    for (size_t i = 0; i < AVX_Size; ++i)
    {
        AVX_Out[i] = _mm256_min_ps(AVX_Mask[i], AVX_Image[i]);
    }
}
EndSeqVectorFloatExecutorWork

BeginSeqVectorFloatExecutorWork(Methods::LightenOnly)
{
    for (size_t i = 0; i < AVX_Size; ++i)
    {
        AVX_Out[i] = _mm256_max_ps(AVX_Mask[i], AVX_Image[i]);
    }
}
EndSeqVectorFloatExecutorWork

BeginSeqVectorFloatExecutorWork(Methods::Multiply)
{
    for (size_t i = 0; i < AVX_Size; ++i)
    {
        AVX_Out[i] = _mm256_mul_ps(AVX_Mask[i], AVX_Image[i]);
    }
}
EndSeqVectorFloatExecutorWork

BeginSeqVectorFloatExecutorWork(Methods::Subtract)
{
    __m256 zero = _mm256_set1_ps(0.0F);

    for (size_t i = 0; i < AVX_Size; ++i)
    {
        __m256 sub = _mm256_sub_ps(AVX_Image[i], AVX_Mask[i]);
        AVX_Out[i] = _mm256_max_ps(sub, zero);
    }
}
EndSeqVectorFloatExecutorWork

BeginSeqVectorFloatExecutorWork(Methods::Screen)
{
    __m256 one = _mm256_set1_ps(1.0F);

    for (size_t i = 0; i < AVX_Size; ++i)
    {
        __m256 MM = _mm256_sub_ps(one, AVX_Mask[i]);
        __m256 II = _mm256_sub_ps(one, AVX_Image[i]);

        __m256 mul = _mm256_mul_ps(MM, II);

        AVX_Out[i] = _mm256_sub_ps(one, mul);
    }
}
EndSeqVectorFloatExecutorWork

BeginSeqVectorFloatExecutorWork(Methods::Diff)
{
    __m256 abs_mask = _mm256_set1_ps(-0.0F);

    for (size_t i = 0; i < AVX_Size; ++i)
    {
        __m256 sub = _mm256_sub_ps(AVX_Image[i], AVX_Mask[i]);

        // unset sign bit
        AVX_Out[i] = _mm256_andnot_ps(abs_mask, sub);
    }
}
EndSeqVectorFloatExecutorWork

BeginSeqVectorFloatExecutorWork(Methods::GrainExtract)
{
    __m256 fraction = _mm256_set1_ps(128.0F / 255.0F);
    __m256 zero     = _mm256_set1_ps(0.0F);
    __m256 one      = _mm256_set1_ps(1.0F);

    for (size_t i = 0; i < AVX_Size; ++i)
    {
        __m256 res = _mm256_add_ps(_mm256_sub_ps(AVX_Image[i], AVX_Mask[i]), fraction);

        AVX_Out[i] = clamp_ps(res, zero, one);
    }
}
EndSeqVectorFloatExecutorWork

BeginSeqVectorFloatExecutorWork(Methods::GrainMerge)
{
    __m256 fraction = _mm256_set1_ps(128.0F / 255.0F);
    __m256 zero     = _mm256_set1_ps(0.0F);
    __m256 one      = _mm256_set1_ps(1.0F);

    for (size_t i = 0; i < AVX_Size; ++i)
    {
        __m256 res = _mm256_sub_ps(_mm256_add_ps(AVX_Image[i], AVX_Mask[i]), fraction);

        AVX_Out[i] = clamp_ps(res, zero, one);
    }
}
EndSeqVectorFloatExecutorWork
