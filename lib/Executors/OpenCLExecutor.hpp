#ifndef LIB_EXECUTORS_OPENCLEXECUTOR_HPP
#define LIB_EXECUTORS_OPENCLEXECUTOR_HPP

#define DefineOpenCLExecutor(Mod, Method)                                                          \
    template<>                                                                                     \
    class Executor<Method, Mod, Platforms::OpenCL> : public ExecutorOpenCL<Mod>                    \
    {                                                                                              \
    public:                                                                                        \
        Executor()                                                                                 \
            : ExecutorOpenCL<Mod>(sourceCode, "EntryFunction")                                     \
        {                                                                                          \
        }                                                                                          \
                                                                                                   \
        static const char *const sourceCode;                                                       \
    }


namespace Lib
{
    class ExecutorOpenCLBase
    {
    protected:
        ExecutorOpenCLBase(std::string_view sourceCode, const char *entryPoint);

        cl::Device m_device;
        cl::Context m_context;

        cl::Program m_program;
        cl::Kernel m_kernel;

        cl::CommandQueue m_queue;
    };

    template<Mods mod>
    class ExecutorOpenCL : public ExecutorOpenCLBase
    {
    public:
        using Pointer      = typename ImageTraits<mod>::ColorType *;
        using ConstPointer = const typename ImageTraits<mod>::ColorType *;
        void work(ConstPointer image, ConstPointer mask, Pointer outImage, std::size_t size);

    protected:
        ExecutorOpenCL(std::string_view sourceCode, const char *entryPoint)
            : ExecutorOpenCLBase(sourceCode, entryPoint)
        {
        }
    };

    DefineOpenCLExecutor(Mods::Integer, Methods::Addition);
    DefineOpenCLExecutor(Mods::Integer, Methods::DarkenOnly);
    DefineOpenCLExecutor(Mods::Integer, Methods::LightenOnly);
    DefineOpenCLExecutor(Mods::Integer, Methods::Multiply);
    DefineOpenCLExecutor(Mods::Integer, Methods::Subtract);
    DefineOpenCLExecutor(Mods::Integer, Methods::Screen);
    DefineOpenCLExecutor(Mods::Integer, Methods::Diff);
    DefineOpenCLExecutor(Mods::Integer, Methods::GrainExtract);
    DefineOpenCLExecutor(Mods::Integer, Methods::GrainMerge);

    DefineOpenCLExecutor(Mods::Float, Methods::Addition);
    DefineOpenCLExecutor(Mods::Float, Methods::DarkenOnly);
    DefineOpenCLExecutor(Mods::Float, Methods::LightenOnly);
    DefineOpenCLExecutor(Mods::Float, Methods::Multiply);
    DefineOpenCLExecutor(Mods::Float, Methods::Subtract);
    DefineOpenCLExecutor(Mods::Float, Methods::Screen);
    DefineOpenCLExecutor(Mods::Float, Methods::Diff);
    DefineOpenCLExecutor(Mods::Float, Methods::GrainExtract);
    DefineOpenCLExecutor(Mods::Float, Methods::GrainMerge);
}


#endif //LIB_EXECUTORS_OPENCLEXECUTOR_HPP
