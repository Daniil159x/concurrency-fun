#include "../Interface.hpp"
#include "OpenCLExecutor.hpp"


using namespace Lib;

std::pair<cl::Context, cl::Device> getDefaultGPU()
{
    auto platform = cl::Platform::getDefault();

    std::vector<cl::Device> devices;
    platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);

    if (devices.empty())
    {
        throw std::runtime_error("Default gpu device not found");
    }

    std::array<cl_context_properties, 3> props
        = { CL_CONTEXT_PLATFORM, reinterpret_cast<cl_context_properties>(platform()), 0 };

    cl::Context context(devices[0], props.data());

    return { context, devices[0] };
}

ExecutorOpenCLBase::ExecutorOpenCLBase(std::string_view sourceCode, const char *entryPoint)
{
    std::tie(m_context, m_device) = getDefaultGPU();

    m_program = cl::Program(m_context, std::string(sourceCode));
    m_program.build({ m_device });

    m_kernel = cl::Kernel(m_program, entryPoint);

    m_queue = cl::CommandQueue(m_context, m_device);
}


template<Mods mod>
void ExecutorOpenCL<mod>::work(ConstPointer image,
                               ConstPointer mask,
                               Pointer outImage,
                               std::size_t size)
{
    size_t dataSize = size * sizeof(typename ImageTraits<mod>::ColorType);

    auto buffImage = cl::Buffer(m_context,
                                CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                dataSize,
                                const_cast<Pointer>(image));

    auto buffMask = cl::Buffer(m_context,
                               CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                               dataSize,
                               const_cast<Pointer>(mask));

    auto buffOut = cl::Buffer(m_context, CL_MEM_WRITE_ONLY, dataSize, nullptr);

    m_kernel.setArg(0, buffImage);
    m_kernel.setArg(1, buffMask);
    m_kernel.setArg(2, buffOut);

    m_queue.enqueueNDRangeKernel(m_kernel, cl::NullRange, cl::NDRange(size));
    m_queue.enqueueReadBuffer(buffOut, true, 0, dataSize, outImage);
}

// Explicitly instantiate
template class Lib::ExecutorOpenCL<Mods::Integer>;
template class Lib::ExecutorOpenCL<Mods::Float>;


#define DefineSourceCode(Mod, Method, ...)                                                         \
    const char *const Executor<Method, Mod, Platforms::OpenCL>::sourceCode = #__VA_ARGS__

// --------------------- Mods::Integer ----------------------------

DefineSourceCode(
    Mods::Integer,
    Methods::Addition,
    kernel void EntryFunction(global unsigned char *image,
                              global unsigned char *mask,
                              global unsigned char *out) {
        size_t i = get_global_id(0);

        int M = mask[i];
        int I = image[i];

        out[i] = (unsigned char)(min(M + I, 255));
    });

DefineSourceCode(
    Mods::Integer,
    Methods::DarkenOnly,
    kernel void EntryFunction(global unsigned char *image,
                              global unsigned char *mask,
                              global unsigned char *out) {
        size_t i = get_global_id(0);
        out[i]   = min(image[i], mask[i]);
    });


DefineSourceCode(
    Mods::Integer,
    Methods::LightenOnly,
    kernel void EntryFunction(global unsigned char *image,
                              global unsigned char *mask,
                              global unsigned char *out) {
        size_t i = get_global_id(0);
        out[i]   = max(image[i], mask[i]);
    });

DefineSourceCode(
    Mods::Integer,
    Methods::Multiply,
    kernel void EntryFunction(global unsigned char *image,
                              global unsigned char *mask,
                              global unsigned char *out) {
        size_t i = get_global_id(0);

        int I = image[i];
        int M = mask[i];

        out[i] = (unsigned char)((I * M) / 255);
    });

DefineSourceCode(
    Mods::Integer,
    Methods::Subtract,
    kernel void EntryFunction(global unsigned char *image,
                              global unsigned char *mask,
                              global unsigned char *out) {
        size_t i = get_global_id(0);

        int I = image[i];
        int M = mask[i];

        out[i] = (unsigned char)(max(I - M, 0));
    });

DefineSourceCode(
    Mods::Integer,
    Methods::Screen,
    kernel void EntryFunction(global unsigned char *image,
                              global unsigned char *mask,
                              global unsigned char *out) {
        size_t i = get_global_id(0);

        int I   = image[i];
        int M   = mask[i];
        int tmp = (255 - M) * (255 - I);
        out[i]  = (unsigned char)(255 - tmp / 255);
    });

DefineSourceCode(
    Mods::Integer,
    Methods::Diff,
    kernel void EntryFunction(global unsigned char *image,
                              global unsigned char *mask,
                              global unsigned char *out) {
        size_t i = get_global_id(0);

        int I = image[i];
        int M = mask[i];

        out[i] = (unsigned char)(abs(I - M));
    });

DefineSourceCode(
    Mods::Integer,
    Methods::GrainExtract,
    kernel void EntryFunction(global unsigned char *image,
                              global unsigned char *mask,
                              global unsigned char *out) {
        size_t i = get_global_id(0);

        int I = image[i];
        int M = mask[i];

        out[i] = (unsigned char)(clamp(I - M + 128, 0, 255));
    });

DefineSourceCode(
    Mods::Integer,
    Methods::GrainMerge,
    kernel void EntryFunction(global unsigned char *image,
                              global unsigned char *mask,
                              global unsigned char *out) {
        size_t i = get_global_id(0);

        int I = image[i];
        int M = mask[i];

        out[i] = (unsigned char)(clamp(I + M - 128, 0, 255));
    });

// --------------------- Mods::Float ----------------------------

DefineSourceCode(
    Mods::Float,
    Methods::Addition,
    kernel void EntryFunction(global float *image, global float *mask, global float *out) {
        size_t i = get_global_id(0);
        out[i]   = min(image[i] + mask[i], 1.0F);
    });

DefineSourceCode(
    Mods::Float,
    Methods::DarkenOnly,
    kernel void EntryFunction(global float *image, global float *mask, global float *out) {
        size_t i = get_global_id(0);
        out[i]   = min(image[i], mask[i]);
    });

DefineSourceCode(
    Mods::Float,
    Methods::LightenOnly,
    kernel void EntryFunction(global float *image, global float *mask, global float *out) {
        size_t i = get_global_id(0);
        out[i]   = max(image[i], mask[i]);
    });

DefineSourceCode(
    Mods::Float,
    Methods::Multiply,
    kernel void EntryFunction(global float *image, global float *mask, global float *out) {
        size_t i = get_global_id(0);
        out[i]   = image[i] * mask[i];
    });

DefineSourceCode(
    Mods::Float,
    Methods::Subtract,
    kernel void EntryFunction(global float *image, global float *mask, global float *out) {
        size_t i = get_global_id(0);
        out[i]   = max(image[i] - mask[i], 0.0F);
    });

DefineSourceCode(
    Mods::Float,
    Methods::Screen,
    kernel void EntryFunction(global float *image, global float *mask, global float *out) {
        size_t i = get_global_id(0);
        out[i]   = 1.0F - (1.0F - mask[i]) * (1.0F - image[i]);
    });

DefineSourceCode(
    Mods::Float,
    Methods::Diff,
    kernel void EntryFunction(global float *image, global float *mask, global float *out) {
        size_t i = get_global_id(0);
        out[i]   = fabs(image[i] - mask[i]);
    });

DefineSourceCode(
    Mods::Float,
    Methods::GrainExtract,
    kernel void EntryFunction(global float *image, global float *mask, global float *out) {
        size_t i = get_global_id(0);
        out[i]   = clamp(image[i] - mask[i] + 128.0F / 255.0F, 0.0F, 1.0F);
    });

DefineSourceCode(
    Mods::Float,
    Methods::GrainMerge,
    kernel void EntryFunction(global float *image, global float *mask, global float *out) {
        size_t i = get_global_id(0);
        out[i]   = clamp(image[i] + mask[i] - 128.0F / 255.0F, 0.0F, 1.0F);
    });
