#ifndef LIB_EXECUTORS_SEQVECEXECUTOR_HPP
#define LIB_EXECUTORS_SEQVECEXECUTOR_HPP

#define DefineSeqVectorExecutor(Mod, Method)                                                       \
    template<>                                                                                     \
    class Executor<Method, Mod, Platforms::SeqVector>                                              \
        : protected Executor<Method, Mod, Platforms::Seq>                                          \
    {                                                                                              \
    public:                                                                                        \
        using BaseExecutor = Executor<Method, Mod, Platforms::Seq>;                                \
        using Pointer      = ImageTraits<Mod>::ColorType *;                                        \
        using ConstPointer = const ImageTraits<Mod>::ColorType *;                                  \
        void work(ConstPointer image, ConstPointer mask, Pointer outImage, std::size_t size);      \
    }


namespace Lib
{
    DefineSeqVectorExecutor(Mods::Integer, Methods::Addition);
    DefineSeqVectorExecutor(Mods::Integer, Methods::DarkenOnly);
    DefineSeqVectorExecutor(Mods::Integer, Methods::LightenOnly);
    DefineSeqVectorExecutor(Mods::Integer, Methods::Multiply);
    DefineSeqVectorExecutor(Mods::Integer, Methods::Subtract);
    DefineSeqVectorExecutor(Mods::Integer, Methods::Screen);
    DefineSeqVectorExecutor(Mods::Integer, Methods::Diff);
    DefineSeqVectorExecutor(Mods::Integer, Methods::GrainExtract);
    DefineSeqVectorExecutor(Mods::Integer, Methods::GrainMerge);

    DefineSeqVectorExecutor(Mods::Float, Methods::Addition);
    DefineSeqVectorExecutor(Mods::Float, Methods::DarkenOnly);
    DefineSeqVectorExecutor(Mods::Float, Methods::LightenOnly);
    DefineSeqVectorExecutor(Mods::Float, Methods::Multiply);
    DefineSeqVectorExecutor(Mods::Float, Methods::Subtract);
    DefineSeqVectorExecutor(Mods::Float, Methods::Screen);
    DefineSeqVectorExecutor(Mods::Float, Methods::Diff);
    DefineSeqVectorExecutor(Mods::Float, Methods::GrainExtract);
    DefineSeqVectorExecutor(Mods::Float, Methods::GrainMerge);
}

#undef DefineSeqVectorExecutor

#endif //LIB_EXECUTORS_SEQVECEXECUTOR_HPP
