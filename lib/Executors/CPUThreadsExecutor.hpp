#ifndef LIB_EXECUTORS_CPUTHREADSEXECUTOR_HPP
#define LIB_EXECUTORS_CPUTHREADSEXECUTOR_HPP

namespace Lib
{
    template<Methods method, Mods mod>
    class Executor<method, mod, Platforms::CPUThreads>
        : protected Executor<method, mod, Platforms::Seq>
    {
    public:
        using Pointer      = typename ImageTraits<mod>::ColorType *;
        using ConstPointer = const typename ImageTraits<mod>::ColorType *;
        using BaseExecutor = Executor<method, mod, Platforms::Seq>;

        static_assert(method < Methods::Number && mod < Mods::Number, "Only valid params allowed");

        explicit Executor(size_t threadsNumber = 0)
        {
            if(threadsNumber == 0)
            {
                threadsNumber = std::thread::hardware_concurrency();
            }

            if(threadsNumber == 0)
            {
                threadsNumber = 6;
            }

            m_pool.resize(threadsNumber);
        }

        void work(ConstPointer image, ConstPointer mask, Pointer outImage, std::size_t size)
        {
            size_t chunkSize   = size / m_pool.size();
            BaseExecutor *This = this;
            for (size_t i = 0; i < m_pool.size(); ++i)
            {
                size_t offset = chunkSize * i;
                m_pool[i]     = std::thread(&BaseExecutor::work,
                                            This,
                                            image + offset,
                                            mask + offset,
                                            outImage + offset,
                                            chunkSize);
            }

            size_t offset = chunkSize * m_pool.size();
            BaseExecutor::work(image + offset, mask + offset, outImage + offset, size - offset);

            for (auto &t : m_pool)
            {
                if (t.joinable())
                {
                    t.join();
                }
            }
        }

    private:
        std::vector<std::thread> m_pool;
    };
}

#undef DefineExecutor

#endif //LIB_EXECUTORS_CPUTHREADSEXECUTOR_HPP
