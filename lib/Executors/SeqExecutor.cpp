#include "../Interface.hpp"

using namespace Lib;

#define DefineSeqExecutorWork(Method, Mod)                                                         \
    void Executor<Method, Mod, Platforms::Seq>::work(Executor::ConstPointer image,                 \
                                                     Executor::ConstPointer mask,                  \
                                                     Executor::Pointer out,                        \
                                                     std::size_t size)


// ---------------------   Mods::Integer ----------------------------

DefineSeqExecutorWork(Methods::Addition, Mods::Integer)
{
    for (size_t i = 0; i < size; ++i)
    {
        int M = mask[i];
        int I = image[i];

        out[i] = static_cast<unsigned char>(std::min(M + I, 255));
    }
}

DefineSeqExecutorWork(Methods::DarkenOnly, Mods::Integer)
{
    for (size_t i = 0; i < size; ++i)
    {
        out[i] = std::min(image[i], mask[i]);
    }
}

DefineSeqExecutorWork(Methods::LightenOnly, Mods::Integer)
{
    for (size_t i = 0; i < size; ++i)
    {
        out[i] = std::max(image[i], mask[i]);
    }
}

DefineSeqExecutorWork(Methods::Multiply, Mods::Integer)
{
    for (size_t i = 0; i < size; ++i)
    {
        int I  = image[i];
        int M  = mask[i];
        out[i] = static_cast<unsigned char>((I * M) / 255);
    }
}

DefineSeqExecutorWork(Methods::Subtract, Mods::Integer)
{
    for (size_t i = 0; i < size; ++i)
    {
        int I  = image[i];
        int M  = mask[i];
        out[i] = static_cast<unsigned char>(std::max(I - M, 0));
    }
}

DefineSeqExecutorWork(Methods::Screen, Mods::Integer)
{
    for (size_t i = 0; i < size; ++i)
    {
        int I     = image[i];
        int M     = mask[i];
        int tmp = (255 - M) * (255 - I);
        out[i]    = static_cast<unsigned char>(255 - tmp / 255);
    }
}

DefineSeqExecutorWork(Methods::Diff, Mods::Integer)
{
    for (size_t i = 0; i < size; ++i)
    {
        int I  = image[i];
        int M  = mask[i];
        out[i] = static_cast<unsigned char>(std::abs(I - M));
    }
}

DefineSeqExecutorWork(Methods::GrainExtract, Mods::Integer)
{
    for (size_t i = 0; i < size; ++i)
    {
        int I  = image[i];
        int M  = mask[i];
        out[i] = static_cast<unsigned char>(std::clamp(I - M + 128, 0, 255));
    }
}

DefineSeqExecutorWork(Methods::GrainMerge, Mods::Integer)
{
    for (size_t i = 0; i < size; ++i)
    {
        int I  = image[i];
        int M  = mask[i];
        out[i] = static_cast<unsigned char>(std::clamp(I + M - 128, 0, 255));
    }
}

// ---------------------   Mods::Float ----------------------------

DefineSeqExecutorWork(Methods::Addition, Mods::Float)
{
    for (size_t i = 0; i < size; ++i)
    {
        out[i] = std::min(image[i] + mask[i], 1.0F);
    }
}

DefineSeqExecutorWork(Methods::DarkenOnly, Mods::Float)
{
    for (size_t i = 0; i < size; ++i)
    {
        out[i] = std::min(image[i], mask[i]);
    }
}

DefineSeqExecutorWork(Methods::LightenOnly, Mods::Float)
{
    for (size_t i = 0; i < size; ++i)
    {
        out[i] = std::max(image[i], mask[i]);
    }
}

DefineSeqExecutorWork(Methods::Multiply, Mods::Float)
{
    for (size_t i = 0; i < size; ++i)
    {
        out[i] = image[i] * mask[i];
    }
}

DefineSeqExecutorWork(Methods::Subtract, Mods::Float)
{
    for (size_t i = 0; i < size; ++i)
    {
        out[i] = std::max(image[i] - mask[i], 0.0F);
    }
}

DefineSeqExecutorWork(Methods::Screen, Mods::Float)
{
    for (size_t i = 0; i < size; ++i)
    {
        out[i] = 1.0F - (1.0F - mask[i]) * (1.0F - image[i]);
    }
}

DefineSeqExecutorWork(Methods::Diff, Mods::Float)
{
    for (size_t i = 0; i < size; ++i)
    {
        out[i] = std::abs(image[i] - mask[i]);
    }
}

DefineSeqExecutorWork(Methods::GrainExtract, Mods::Float)
{
    for (size_t i = 0; i < size; ++i)
    {
        out[i] = std::clamp(image[i] - mask[i] + 128.0F / 255.0F, 0.0F, 1.0F);
    }
}

DefineSeqExecutorWork(Methods::GrainMerge, Mods::Float)
{
    for (size_t i = 0; i < size; ++i)
    {
        out[i] = std::clamp(image[i] + mask[i] - 128.0F / 255.0F, 0.0F, 1.0F);
    }
}
